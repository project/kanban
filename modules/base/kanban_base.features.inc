<?php
/**
 * @file
 * kanban_base.features.inc
 */

/**
 * Implements hook_node_info().
 */
function kanban_base_node_info() {
  $items = array(
    'kanban_process' => array(
      'name' => t('Kanban Process'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Kanban Process Title'),
      'help' => '',
    ),
    'kanban_step' => array(
      'name' => t('Kanban Step'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Kanban Step Title'),
      'help' => '',
    ),
    'kanban_task' => array(
      'name' => t('Kanban Task'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
