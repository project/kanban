DESCRIPTION
--------------------------
This module is providing possibilities to create kanban workflows primary with a
logic of processes (swimlane), steps (slot in a swimlane) and tasks (cards).
This are node types connected with entity references.
There are special links on provided task/step lists to trigger a rule for change
the task-step relation directly. If in the relted process confirmation is
not activated. The change is done immediately if no WIP Limit (max tasks in
step) will stop this.
In future there will be some more possibilities to control this with some rules
conditions.

Kanban is primary developed to work with organic groups. That is a ajor task.
But it should work without this.


INSTALLATION
--------------------------
Just install the main module and its dependencies.
Activate the three kanban blocks to show kanban information on the three kanban
node types "kanban_process, kanban_steps and kanban_tasks" which are installed
by the "kanban_base.module" (features).


FUTURE PLAN (sub modules):
--------------------------
+ kanban_activity.module
  Manage special activities and activity schemata (ToDo's) with own entities.
+ kanban_time.module
  Manage timetracker informations.
+ kanban_track.module
  Manage logging of all changes in kanban processes and related data.
