<?php
/**
 * @file
 * kanban-block-task-steps.tpl.php
 *
 * Variables available:
 * - $data: an array of data.
 *    $data['processlink'] = link to process node.
 *    $data['steps'] = array of step data.
 */
?>

<div class="kanban-task-steps">
    <h3><?php echo t('process'); ?>: <?php echo $data['processlink']; ?></h3>
  <ul class="links clearfix">

  <?php foreach ($data['steps'] as $step):?>

    <li class="<?php echo $step['classes']; ?>" >
      <h4><a href="<?php echo $step['steplink'];?>" title="<?php echo t('go to step');?>">
        <?php echo check_markup($step['query']->title);?></a></h4>


      <?php if ($step['switch_link'] == ''):?>
      <div class="kanban-switch-nolink">
       <span><?php echo t('step');?></span>
      </div>

      <?php elseif ($step['switch_link'] == 'current'):?>
        <div class="kanban-switch-current">
        <span><?php echo t('current');?></span>
        </div>
       <?php else:?>
        <div class="kanban-switch-link">
        <a href="<?php echo $step['switch_link'];?>" title="<?php echo t('move task to step');?>">
          <span><?php echo t('switch');?></span>
        </a>
        </div>
        <?php endif;?>

    <?php endforeach;?>
  </ul>
</div>
