<?php
/**
 * @file
 * The theme system, which controls the kanban task view.
 */

/**
 * Display a view in special kanban style.
 */
function template_preprocess_kanban_processtasks(&$vars) {
  drupal_add_css(drupal_get_path('module', 'kanban') . '/theme/kanban-processtasks.css');

  $view     = $vars['view'];
  $result   = $view->result;
  // Currently not used views informations.
  // ToDo: Delete if not needed.
  // $views_options  = $view->style_plugin->options;
  // $handler  = $view->style_plugin;
  $args = $view->args;
  $process_id = 0;
  if ($args[0]) {
    $process_id = $args[0];
  }

  // Maybe usefil in future but currently unused:
  // $request_path = request_path();

  $data = array();

  $process_node = node_load($process_id);

  $steps = _kanban_get_process_steps($process_id, TRUE);
  $steps_count = count($steps);

  // To get steps by its position as key.
  $steps_sort = array();
  $step_counter = 0;
  foreach ($steps as $step_id => $step) {
    $step_counter++;

    $data[$step_id] = array();
    $data[$step_id]['classes'] = 'kanban-step';
    if ($step_counter == 1) {
      $data[$step_id]['classes'] .= ' kanban-step-first';
    }
    if ($step_counter == $steps_count) {
      $data[$step_id]['classes'] .= ' kanban-step-last';
    }
    $data[$step_id]['query'] = $step['query'];
    $steps_sort[$step['position']] = $step;
    $data[$step_id]['tasks'] = array();
    $data[$step_id]['link'] = url("node/$step_id");
  }

  $count = 0;
  foreach ($vars['rows'] as $row_index => $item) {
    $count++;

    $task = array();
    $task['views-item'] = $item;
    $task['classes'] = 'kanban-task';

    $task_node = $result[$row_index]->_field_data['nid']['entity'];
    $task['node'] = $task_node;
    $task_id = $task_node->nid;
    $task['link'] = url("node/$task_id");

    global $user;
    $task['write_access'] = FALSE;
    if (_kanban_access_task_move($task_node, $user)) {
      $task['write_access'] = TRUE;
    }

    $step_id = $task_node->field_kanban_step_ref[LANGUAGE_NONE][0]['target_id'];
    $task['step_id'] = $step_id;

    $task['before'] = '';
    $task['next'] = '';

    if (($steps[$step_id]['position'] > 1) || ($steps[$step_id]['position'] < $steps_count)) {
      $query = array(
        'destination' => current_path(),
        'token' => _kanban_get_token($task_id),
      );
      $options = array('query' => $query);
      if ($task['write_access']) {
        if ($steps[$step_id]['position'] > 1) {
          $step_before_position = $steps[$step_id]['position'] - 1;
          $step_before_id = $steps_sort[$step_before_position]['id'];
          if ($process_node->field_kanban_confirm[LANGUAGE_NONE][0]['value'] == 1) {
            $task['before'] = url("kanban/change/task/$task_id/step/$step_before_id/confirm", $options);
          }
          else {
            $task['before'] = url("kanban/change/task/$task_id/step/$step_before_id", $options);
          }

        }
        if ($steps[$step_id]['position'] < $steps_count) {
          $step_next_position = $steps[$step_id]['position'] + 1;
          $step_next_id = $steps_sort[$step_next_position]['id'];
          if ($process_node->field_kanban_confirm[LANGUAGE_NONE][0]['value'] == 1) {
            $task['next'] = url("kanban/change/task/$task_id/step/$step_next_id/confirm", $options);
          }
          else {
            $task['next'] = url("kanban/change/task/$task_id/step/$step_next_id", $options);
          }
        }
      }
    }
    if (($task['before'] != '') && ($task['next'] != '')) {
      $task['classes'] .= ' kanban-next-before';
    }

    $data[$step_id]['tasks'][$task_id] = $task;
  }

  foreach ($data as $step_id => $step) {
    $data[$step_id]['maxtasks'] = 0;
    $data[$step_id]['wiplimit'] = 0;

    if ($step['query']->field_kanban_limit_value != '') {
      $data[$step_id]['wiplimit'] = $step['query']->field_kanban_limit_value;
    }

    $data[$step_id]['tasks_sum'] = count($step['tasks']);
    if ($data[$step_id]['wiplimit'] > 0) {
      if ($data[$step_id]['wiplimit'] == $data[$step_id]['tasks_sum']) {
        $data[$step_id]['maxtasks'] = 1;
        $data[$step_id]['classes'] .= ' kanban-step-maxtasks-1';
      }
      elseif ($data[$step_id]['wiplimit'] > $data[$step_id]['tasks_sum']) {
        $data[$step_id]['maxtasks'] = 2;
        $data[$step_id]['classes'] .= ' kanban-step-maxtasks-2';
      }
    }
    $step_info = '';
    $step_info .= $data[$step_id]['tasks_sum'];
    if ($data[$step_id]['tasks_sum'] = 1) {
      $step_info .= ' ' . t('task');
    }
    else {
      $step_info .= ' ' . t('tasks');
    }
    if ($data[$step_id]['wiplimit'] != '0') {
      $step_info .= ' | ' . t('WIP Limit: %wip', (array('%wip ' => $data[$step_id]['wiplimit'])));
    }
    $data[$step_id]['step_info'] = $step_info;
  }

  $vars['data'] = $data;
}
