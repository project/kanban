<?php
/**
 * @file
 * kanban-processtasks.tpl.php
 *
 * Default kanban view template to display tasks in kanban style.
 */
?>

<div class="kanban-project-tasks links clearfix">
  <ul class="kanban-steplist">
   <?php foreach ($data as $step):?>
    <li class="<?php print $step['classes'];?>">
      <h3><a href="<?php echo $step['link'];?>"><?php echo check_plain($step['query']->title);?></a></h3>
      <div class="kanban-step-info">
        <?php echo $step['step_info'];?>
      </div>

      <ul class="kanban-tasklist">
      <?php foreach ($step['tasks'] as $task):?>
      <li class="<?php print $task['classes'];?>">

        <?php echo $task['views-item'];?>
        <div class="kanban-task-links">

        <?php if ($task['before'] != ''):?>
        <a class="kanban-tasklist-before"
           href="<?php echo $task['before'];?>"
           title="<?php echo t('move task to step before');?>"
           >&lt;<span><?php echo t('switch to step before');?></span></a>
        <?php endif; ?>

        <?php if ($task['next'] != ''):?>
        <a class="kanban-task-link-next"
           href="<?php echo $task['next'];?>"
           title="<?php echo t('move task to next step');?>"
           >&gt;<span><?php echo t('switch to step next');?></span></a>
           <?php endif;?>
        </div>

      </li>
      <?php endforeach;?>
        </ul>
    </li>
    <?php endforeach;?>
  </ul>
</div>
