<?php
/**
 * @file
 * Provide usful rule sets for kanban.module.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function kanban_default_rules_configuration() {
  $items = array();

  $items['rules_kanban_task_change'] = entity_import('rules_config', '{ "rules_kanban_task_change" : {
      "LABEL" : "kanban task change",
      "PLUGIN" : "reaction rule",
      "REQUIRES" : [ "rules", "kanban" ],
      "ON" : [ "kanban_change_task_step_request" ],
      "DO" : [
        { "kanban_change_task_step_exec" : { "task_id" : "[task-node:nid]", "step_id" : "[step-node:nid]" } }
      ]
    }
  }');

  return $items;
}
