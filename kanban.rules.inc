<?php
/**
 * @file
 * Rules integration for kanban.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_event_info().
 */
function kanban_rules_event_info() {

  $variables = array(
    'task_node' => array(
      'type' => 'node',
      'label' => t('Task node'),
    ),
    'step_node' => array(
      'label' => t('Step node'),
      'type' => 'node',
    ),
  );
  $items = array();

  $items['kanban_change_task_step_request'] = array(
    'label' => t('kanban change request task to step'),
    'group' => t('Kanban'),
    'variables' => $variables,
    'access callback' => 'user_access',
    'access arguments' => array('use kanban tasks'),
  );

  return $items;
}

/**
 * Implements hook_rules_condition_info().
 */
function kanban_rules_condition_info() {
  $conditions = array();
  // ToDo: Add some useful conditions.
  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function kanban_rules_action_info() {
  $actions = array();

  $actions['kanban_change_task_step_exec'] = array(
    'label' => t('Change a task reference to step.'),
    'parameter' => array(
      'task_id' => array(
        'type' => 'text',
        'label' => t('Task ID'),
        'description' => t('The Task to be changed'),
      ),
      'step_id' => array(
        'type' => 'text',
        'label' => t('Step ID'),
        'description' => t('The ID of the step where the task should be connected to.'),
      ),
    ),
    'group' => t('Kanban'),
    'callbacks' => array(
      'execute' => '_kanban_change_task_step_exec',
    ),
  );


  return $actions;
}

/**
 * @}
 */
