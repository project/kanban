<?php
/**
 * @file
 * Page callbacks for kanban.module.
 */

/**
 * Page callback for kanban/change/task/%/step/%/confirm.
 */
function _kanban_change_task_step_confirm($task_id = 0, $step_id = 0) {
  if (!(is_numeric($task_id) && is_numeric($step_id))) {
    return drupal_access_denied();
  }
  $options = array();
  if (isset($_GET['destination'])) {
    $query = array(
      'destination' => $_GET['destination'],
      'token' => $_GET['token'],
    );

    $options = array('query' => $query);
  }
  $link_title = t('change the step of task!');
  $link_url = "kanban/change/task/$task_id/step/$step_id";
  $switch_link = l($link_title, $link_url, $options);

  return $switch_link;

}
