<?php

/**
 * @file
 * Implementaion of the views hooks.
 */

/**
 * Implements hook_views_plugins().
 */
function kanban_views_plugins() {
  $path = drupal_get_path('module', 'kanban');

  return array(
    'module' => 'kanban',
    'style' => array(
      'kanban_processtasks' => array(
        'title' => t('Kanban process tasks grid'),
        'help' => t('Display the kanban process tasks in a kanban grid.'),
        'handler' => 'KanbanPluginStyleProcesstasks',
        'path' => "$path/includes/views",
        'type' => 'normal',
        'theme' => 'kanban_processtasks',
        'theme path' => "$path/theme",
        'theme file' => 'kanban.theme.inc',
        'uses row plugin' => TRUE,
        'uses fields' => TRUE,
        'uses grouping' => FALSE,
        'uses options' => TRUE,
        'even empty' => 'even empty',
      ),
    ),
  );
}
