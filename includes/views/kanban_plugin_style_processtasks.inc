<?php
/**
 * @file
 * kanban_plugin_style_processtasks.inc
 */

/**
 * This class holds all the funtionality used for the unformatted style plugin.
 */
class KanbanPluginStyleProcesstasks extends views_plugin_style {

  /**
   * Set default options.
   */
  public function optionDefinition() {
    $options = parent::option_definition();

    $options['data_theme'] = array('default' => 'default');

    return $options;
  }

  /**
   * Render the given style.
   */
  public function optionsForm(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $form['data_theme'] = array(
      '#title' => t('Theme class suffix'),
      '#description' => t('String to be added at some classes. Default is "default".'),
      '#type' => 'textfield',
      '#size' => '30',
      '#default_value' => $this->options['data_theme'],
    );

  }

}
