<?php

/**
 * @file
 * Default views for kanban.module.
 */

/**
 * Implements hook_views_default_views().
 */
function kanban_views_default_views() {
  $views = array();

  $view = new view();
  $view->name = 'kanban_process_steps';
  $view->description = '';
  $view->tag = 'kanban';
  $view->base_table = 'node';
  $view->human_name = 'kanban process steps';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_kanban_process_ref',
      'rendered' => 0,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['style_options']['columns'] = array(
    'title' => 'title',
    'field_kanban_weight' => 'field_kanban_weight',
    'field_kanban_limit' => 'field_kanban_limit',
    'field_kanban_process_ref' => 'field_kanban_process_ref',
  );
  $handler->display->display_options['style_options']['default'] = 'field_kanban_weight';
  $handler->display->display_options['style_options']['info'] = array(
    'title' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_kanban_weight' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_kanban_limit' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_kanban_process_ref' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 0;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = 'No steps available yet.';
  $handler->display->display_options['empty']['area']['format'] = 'filtered_html';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Step weight in process */
  $handler->display->display_options['fields']['field_kanban_weight']['id'] = 'field_kanban_weight';
  $handler->display->display_options['fields']['field_kanban_weight']['table'] = 'field_data_field_kanban_weight';
  $handler->display->display_options['fields']['field_kanban_weight']['field'] = 'field_kanban_weight';
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_kanban_weight']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_kanban_weight']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_kanban_weight']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_kanban_weight']['type'] = 'editable';
  $handler->display->display_options['fields']['field_kanban_weight']['settings'] = array(
    'click_to_edit' => 1,
    'empty_text' => '',
    'fallback_format' => 'number_integer',
    'fallback_settings' => array(
      'thousand_separator' => ' ',
      'prefix_suffix' => 1,
    ),
  );
  $handler->display->display_options['fields']['field_kanban_weight']['field_api_classes'] = 0;
  /* Field: Content: Step WIP Limit */
  $handler->display->display_options['fields']['field_kanban_limit']['id'] = 'field_kanban_limit';
  $handler->display->display_options['fields']['field_kanban_limit']['table'] = 'field_data_field_kanban_limit';
  $handler->display->display_options['fields']['field_kanban_limit']['field'] = 'field_kanban_limit';
  $handler->display->display_options['fields']['field_kanban_limit']['label'] = 'WIP Limit';
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['element_label_colon'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_kanban_limit']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_kanban_limit']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_kanban_limit']['type'] = 'editable';
  $handler->display->display_options['fields']['field_kanban_limit']['settings'] = array(
    'click_to_edit' => 1,
    'empty_text' => 'unlimited',
    'fallback_format' => 'number_integer',
    'fallback_settings' => array(
      'thousand_separator' => ' ',
      'prefix_suffix' => 1,
    ),
  );
  $handler->display->display_options['fields']['field_kanban_limit']['field_api_classes'] = 0;
  /* Field: Content: Process Reference */
  $handler->display->display_options['fields']['field_kanban_process_ref']['id'] = 'field_kanban_process_ref';
  $handler->display->display_options['fields']['field_kanban_process_ref']['table'] = 'field_data_field_kanban_process_ref';
  $handler->display->display_options['fields']['field_kanban_process_ref']['field'] = 'field_kanban_process_ref';
  $handler->display->display_options['fields']['field_kanban_process_ref']['label'] = '';
  $handler->display->display_options['fields']['field_kanban_process_ref']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_kanban_process_ref']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_kanban_process_ref']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_kanban_process_ref']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_kanban_process_ref']['settings'] = array(
    'link' => 1,
  );
  $handler->display->display_options['fields']['field_kanban_process_ref']['field_api_classes'] = 0;
  /* Sort criterion: Content: Step weight in process (field_kanban_weight) */
  $handler->display->display_options['sorts']['field_kanban_weight_value']['id'] = 'field_kanban_weight_value';
  $handler->display->display_options['sorts']['field_kanban_weight_value']['table'] = 'field_data_field_kanban_weight';
  $handler->display->display_options['sorts']['field_kanban_weight_value']['field'] = 'field_kanban_weight_value';
  /* Contextual filter: Content: Process Reference (field_kanban_process_ref) */
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['id'] = 'field_kanban_process_ref_target_id';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['table'] = 'field_data_field_kanban_process_ref';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['field'] = 'field_kanban_process_ref_target_id';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['default_action'] = 'empty';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['not'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'kanban_step' => 'kanban_step',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'kanban/process/%';

  $views[$view->name] = $view;

  $view = new view();
  $view->name = 'process_tasks';
  $view->description = '';
  $view->tag = 'kanban';
  $view->base_table = 'node';
  $view->human_name = 'kanban process tasks';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'kanban process tasks';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '99';
  $handler->display->display_options['style_plugin'] = 'kanban_processtasks';
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = 0;
  $handler->display->display_options['row_options']['default_field_elements'] = 1;
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_kanban_step_ref_target_id']['id'] = 'field_kanban_step_ref_target_id';
  $handler->display->display_options['relationships']['field_kanban_step_ref_target_id']['table'] = 'field_data_field_kanban_step_ref';
  $handler->display->display_options['relationships']['field_kanban_step_ref_target_id']['field'] = 'field_kanban_step_ref_target_id';
  $handler->display->display_options['relationships']['field_kanban_step_ref_target_id']['required'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Step Reference */
  $handler->display->display_options['fields']['field_kanban_step_ref']['id'] = 'field_kanban_step_ref';
  $handler->display->display_options['fields']['field_kanban_step_ref']['table'] = 'field_data_field_kanban_step_ref';
  $handler->display->display_options['fields']['field_kanban_step_ref']['field'] = 'field_kanban_step_ref';
  $handler->display->display_options['fields']['field_kanban_step_ref']['label'] = '';
  $handler->display->display_options['fields']['field_kanban_step_ref']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_kanban_step_ref']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_kanban_step_ref']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_kanban_step_ref']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_kanban_step_ref']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['fields']['field_kanban_step_ref']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Process Reference (field_kanban_process_ref) */
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['id'] = 'field_kanban_process_ref_target_id';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['table'] = 'field_data_field_kanban_process_ref';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['field'] = 'field_kanban_process_ref_target_id';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['relationship'] = 'field_kanban_step_ref_target_id';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_kanban_process_ref_target_id']['not'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'kanban_task' => 'kanban_task',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');

  $views[$view->name] = $view;

  $view = new view();
  $view->name = 'kanban_step_tasks';
  $view->description = '';
  $view->tag = 'kanban';
  $view->base_table = 'node';
  $view->human_name = 'kanban step tasks';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'kanban step tasks';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Step Reference (field_kanban_step_ref) */
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['id'] = 'field_kanban_step_ref_target_id';
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['table'] = 'field_data_field_kanban_step_ref';
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['field'] = 'field_kanban_step_ref_target_id';
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['break_phrase'] = 0;
  $handler->display->display_options['arguments']['field_kanban_step_ref_target_id']['not'] = 0;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'kanban_task' => 'kanban_task',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');

  $views[$view->name] = $view;
  return $views;
}
