<?php
/**
 * @file
 * Admin page callback.
 */

/**
 * Callback for admin form.
 */
function kanban_settings_form() {
  $options = array();
  $types = node_type_get_types();
  foreach ($types as $type => $info) {
    if (($type != 'kanban_process') && ($type != 'kanban_step')) {
      $options[$type] = $info->name;
    }
  }

  $form = array();
  $form['kanban_nodetypes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Kanban task node types'),
    '#description' => t('Currently not used, but it is helpful to select the node types which you are using as task nodes for future functions of this module.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['kanban_nodetypes']['kanban_nodetype_task'] = array(
    '#type' => 'checkboxes',
    '#title' => t('kanban tasks content/node types.'),
    '#options' => $options,
    '#default_value' => variable_get('kanban_nodetype_task', array('kanban_task' => 'kanban_task')),
    '#description' => t('Choose one or more content types to validate with. Default is "kanban_task" provided by the kanban_base features module. The types "kanban_process" and "kanban_step" can\'t be also a task. So they are deletd from this option list.'),
  );

  return system_settings_form($form);
}
